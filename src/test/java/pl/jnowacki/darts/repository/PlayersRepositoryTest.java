package pl.jnowacki.darts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

/**
 * Created by JNOWACKI on 2017-02-13.
 */


@SpringBootTest
@RunWith(SpringRunner.class)
public class PlayersRepositoryTest
{
    @Autowired
    private PlayersRepository playersRepository;

    @Transactional
    @Test
    public void getAllPlayers()
    {
        playersRepository.findAll().forEach(player ->
                System.out.println(player));
    }
}