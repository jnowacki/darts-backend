package pl.jnowacki.darts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.jnowacki.darts.entity.Game;


import javax.transaction.Transactional;

import static org.junit.Assert.*;

/**
 * Created by JNOWACKI on 2017-02-13.
 */

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class GamesRepositoryTest
{
    @Autowired
    private GamesRepository gamesRepository;

    @Test
    public void getAllGames()
    {
        for (Game game : gamesRepository.findAll())
        {
            System.out.println(game);
        }
    }
}