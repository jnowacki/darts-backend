package pl.jnowacki.darts.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.jnowacki.darts.controller.dto.PlayerDTO;
import pl.jnowacki.darts.controller.dto.mapper.PlayerMapper;
import pl.jnowacki.darts.service.PlayersService;

import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-14.
 */

@RestController
@RequestMapping("/players")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class PlayersController
{
    @NonNull
    private PlayersService playersService;

    @NonNull
    private PlayerMapper playerMapper;

    @RequestMapping(method = RequestMethod.GET)
    public List<PlayerDTO> getAllPlayers()
    {
        return playerMapper.toDTOs(playersService.findAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    public PlayerDTO createPlayer(@RequestBody PlayerDTO playerDTO)
    {
        return playerMapper.toDTO(playersService.save(playerMapper.toPlayer(playerDTO)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PlayerDTO getPlayer(@PathVariable Long id)
    {
        return playerMapper.toDTO(playersService.findOne(id));
    }

    //PUT request just updates name
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public PlayerDTO updatePlayer(@RequestBody PlayerDTO playerDTO, @PathVariable Long id)
    {
        playerDTO.setId(id);

        return playerMapper.toDTO(playersService.save(playerMapper.toPlayer(playerDTO)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePlayer(@PathVariable Long id)
    {
        playersService.delete(id);
    }

}
