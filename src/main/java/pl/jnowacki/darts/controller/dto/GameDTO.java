package pl.jnowacki.darts.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameDTO
{
    @Setter
    @Getter
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @Setter
    @Getter
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    private Date date;

    @Setter
    @Getter
    private List<PlayerDTO> players;

    @Setter
    @Getter
    private List<PlayerDTO> winners;
}
