package pl.jnowacki.darts.controller.dto.mapper;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jnowacki.darts.controller.dto.PlayerDTO;
import pl.jnowacki.darts.entity.Player;
import pl.jnowacki.darts.service.PlayersService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-15.
 */

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public final class PlayerMapper
{
    @NonNull
    private PlayersService playersService;

    private PlayerMapper()
    {
    }

    public PlayerDTO toDTO(Player player)
    {
        PlayerDTO newPlayerDTO = new PlayerDTO();

        newPlayerDTO.setId(player.getId());
        newPlayerDTO.setName(player.getName());

        return newPlayerDTO;
    }

    //If ID exists only name is overwritten
    public Player toPlayer(PlayerDTO playerDTO)
    {
        if (playerDTO.getId() != null)
        {
            Player player = playersService.findOne(playerDTO.getId());
            player.setName(playerDTO.getName());

            return player;
        }

        //Empty player
        Player newPlayer = new Player();

        newPlayer.setName(playerDTO.getName());
        newPlayer.setGames(Collections.emptyList());
        newPlayer.setWonGames(Collections.emptyList());

        return newPlayer;
    }

    public List<PlayerDTO> toDTOs(List<Player> players)
    {
        List<PlayerDTO> playerDTOs = new ArrayList<>(players.size());

        players.forEach(player -> playerDTOs.add(toDTO(player)));

        return playerDTOs;
    }

    public List<Player> toPlayers(List<PlayerDTO> playerDTOs)
    {
        List<Player> players = new ArrayList<>(playerDTOs.size());

        playerDTOs.forEach(playerDTO -> players.add(toPlayer(playerDTO)));

        return players;
    }
}
