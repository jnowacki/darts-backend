package pl.jnowacki.darts.controller.dto.mapper;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jnowacki.darts.controller.dto.GameDTO;
import pl.jnowacki.darts.controller.dto.PlayerDTO;
import pl.jnowacki.darts.entity.Game;
import pl.jnowacki.darts.entity.Player;
import pl.jnowacki.darts.service.GamesService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-15.
 */

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public final class GameMapper
{
    @NonNull
    private GamesService gamesService;

    @NonNull
    private PlayerMapper playerMapper;

    private GameMapper() {}

    //All properties except ID are overwritten
    public Game toGame(GameDTO gameDTO)
    {
        Game newGame;

        if(gameDTO.getId() != null)
            newGame = gamesService.findOne(gameDTO.getId());
        else
            newGame = new Game();

        newGame.setDate(gameDTO.getDate());

        List<Player> newPlayers = new ArrayList<>();
        gameDTO.getPlayers().forEach(playerDTO -> newPlayers.add(playerMapper.toPlayer(playerDTO)));
        newGame.setPlayers(newPlayers);

        List<Player> newWinners = new ArrayList<>();
        gameDTO.getWinners().forEach(playerDTO -> newWinners.add(playerMapper.toPlayer(playerDTO)));
        newGame.setWinners(newWinners);

        return newGame;
    }

    public GameDTO toDTO(Game game)
    {
        GameDTO newGameDTO = new GameDTO();

        newGameDTO.setId(game.getId());

        newGameDTO.setDate(game.getDate());

        List<PlayerDTO> newPlayersDTO = new ArrayList<>();
        game.getPlayers().forEach(player -> newPlayersDTO.add(playerMapper.toDTO(player)));
        newGameDTO.setPlayers(newPlayersDTO);

        List<PlayerDTO> newWinnersDTO = new ArrayList<>();
        game.getWinners().forEach(player -> newWinnersDTO.add(playerMapper.toDTO(player)));
        newGameDTO.setWinners(newWinnersDTO);

        return newGameDTO;
    }

    public List<GameDTO> toDTOs(List<Game> games)
    {
        List<GameDTO> gameDTOs = new ArrayList<>(games.size());

        games.forEach(game -> gameDTOs.add(toDTO(game)));

        return gameDTOs;
    }

    public List<Game> toGames(List<GameDTO> gameDTOs)
    {
        List<Game> games = new ArrayList<>(gameDTOs.size());

        gameDTOs.forEach(gameDTO -> games.add(toGame(gameDTO)));

        return games;
    }
}
