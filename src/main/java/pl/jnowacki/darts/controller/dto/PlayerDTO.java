package pl.jnowacki.darts.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by JNOWACKI on 2017-02-15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerDTO
{
    @Setter
    @Getter
    private Long id;

    @Setter
    @Getter
    private String name;
}
