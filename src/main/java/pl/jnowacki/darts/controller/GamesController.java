package pl.jnowacki.darts.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.jnowacki.darts.controller.dto.GameDTO;
import pl.jnowacki.darts.controller.dto.mapper.GameMapper;
import pl.jnowacki.darts.service.GamesService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-13.
 */

@RestController
@RequestMapping("/games")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class GamesController
{
    @NonNull
    private GamesService gamesService;

    @NonNull
    private GameMapper gameMapper;

    @RequestMapping(method = RequestMethod.GET)
    public List<GameDTO> getAllGames()
    {
        return gameMapper.toDTOs(gamesService.findAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    public GameDTO createGame(@RequestBody GameDTO gameDTO)
    {
        return gameMapper.toDTO(gamesService.save(gameMapper.toGame(gameDTO)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public GameDTO getGame(@PathVariable Long id)
    {
        return gameMapper.toDTO(gamesService.findOne(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public GameDTO updateGame(@RequestBody GameDTO gameDTO, @PathVariable Long id)
    {
        gameDTO.setId(id);

        return gameMapper.toDTO(gamesService.save(gameMapper.toGame(gameDTO)));
    }

    @RequestMapping(method = RequestMethod.GET, params = {"startDate", "endDate"})
    public List<GameDTO> getGamesBetween(@RequestParam String startDate, @RequestParam String endDate) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm");

        return gameMapper.toDTOs(gamesService.findBetweenDates(formatter.parse(startDate), formatter.parse(endDate)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void updateGame(@PathVariable Long id)
    {
        gamesService.delete(id);
    }
}