package pl.jnowacki.darts.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jnowacki.darts.entity.Player;

import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Repository
public interface PlayersRepository extends CrudRepository<Player, Long>
{
    List<Player> findAll();
}