package pl.jnowacki.darts.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jnowacki.darts.entity.Game;

import java.util.Date;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Repository
public interface GamesRepository extends CrudRepository<Game, Long>
{
    List<Game> findAll();

    //Games that occurred during given time range
    @Query("select game from Game game where game.date > ?1 and game.date < ?2 order by game.date asc")
    List<Game> findGamesBetween(Date startTimestamp, Date stopTimestamp);
}
