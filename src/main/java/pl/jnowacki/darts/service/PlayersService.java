package pl.jnowacki.darts.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jnowacki.darts.entity.Player;
import pl.jnowacki.darts.repository.PlayersRepository;

import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class PlayersService
{
    @NonNull
    private PlayersRepository playersRepository;

    public Player save(Player player)
    {
        return playersRepository.save(player);
    }

    public List<Player> findAll()
    {
        return playersRepository.findAll();
    }

    public Player findOne(Long id)
    {
        return playersRepository.findOne(id);
    }

    public void delete (Long id)
    {
        playersRepository.delete(id);
    }
}
