package pl.jnowacki.darts.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jnowacki.darts.entity.Game;
import pl.jnowacki.darts.repository.GamesRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class GamesService
{
    @NonNull
    private GamesRepository gamesRepository;

    public List<Game> findAll()
    {
        return gamesRepository.findAll();
    }

    public Game findOne(Long id)
    {
        return gamesRepository.findOne(id);
    }

    public List<Game> findBetweenDates(Date startDate, Date endDate)
    {
        return gamesRepository.findGamesBetween(startDate, endDate);
    }

    public Game save(Game game)
    {
        return gamesRepository.save(game);
    }

    public void delete(Long id)
    {
        gamesRepository.delete(id);
    }
}
