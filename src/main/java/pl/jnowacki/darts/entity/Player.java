package pl.jnowacki.darts.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Entity
@Table(name = "players")
@NoArgsConstructor
@ToString
public class Player
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter
    @Getter
    private Long id;

    @Column(nullable = false,  unique=true, length = 255)
    @Setter
    @Getter
    private String name;

    @ManyToMany(mappedBy = "players")
    @JsonBackReference(value = "playedGames")
    @Setter
    @Getter
    private List<Game> games;

    @ManyToMany(mappedBy = "winners")
    @JsonBackReference(value = "wonGames")
    @Setter
    @Getter
    private List<Game> wonGames;
}
