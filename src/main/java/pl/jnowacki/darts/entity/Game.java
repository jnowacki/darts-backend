package pl.jnowacki.darts.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by JNOWACKI on 2017-02-10.
 */

@Entity
@Table(name = "games")
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class , property = "id")
public class Game
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter
    @Getter
    private Long id;

    @Column(nullable = false)
    @Setter
    @Getter
    private Date date;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "players_games", schema = "darts_board",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "player_id"))
    @JsonManagedReference(value = "playedGames")
    @Setter
    @Getter
    private List<Player> players;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "winners_games", schema = "darts_board",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "winner_id"))
    @JsonManagedReference(value = "wonGames")
    @Setter
    @Getter
    private List<Player> winners;
}
