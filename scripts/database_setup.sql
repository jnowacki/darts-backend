DROP DATABASE IF EXISTS `darts_board`;

create database `darts_board` 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;
use `darts_board`;

CREATE TABLE `players`(
`id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(255) NOT NULL UNIQUE
)
ENGINE = InnoDB;

CREATE TABLE `games`(
`id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
`date`  DATETIME NOT NULL
)
ENGINE = InnoDB;

CREATE TABLE `winners_games`(
`winner_id` INT UNSIGNED NOT NULL,
`game_id` INT UNSIGNED NOT NULL,
PRIMARY KEY (`winner_id`, `game_id`),
FOREIGN KEY (`winner_id`) REFERENCES `players`(`id`),
FOREIGN KEY (`game_id`) REFERENCES `games`(`id`)
)
ENGINE = InnoDB;

CREATE TABLE `players_games`(
`player_id` INT UNSIGNED NOT NULL,
`game_id` INT UNSIGNED NOT NULL,
PRIMARY KEY (`player_id`, `game_id`),
FOREIGN KEY (`player_id`) REFERENCES `players`(`id`),
FOREIGN KEY (`game_id`) REFERENCES `games`(`id`)
)
ENGINE = InnoDB;

INSERT INTO `darts_board`.`players` (`name`) VALUES 
("Jędrzej N."),
("Kuba W."),
("Kamil K."),
("Mateusz M."),
("Maciej M."),
("Krzysztof M."),
("Tomek K."),
("Adam R."),
("Canko C.");

select * from players